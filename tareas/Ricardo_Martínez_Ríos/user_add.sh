#!/bin/bash

if [[ $# -eq 0 ]]; then
	echo "No arguments..."
	exit 1
fi

FILE=$1

if [[ ! -r $FILE ]]; then
	echo "File can't be found/is not readable..."
	exit 1
fi

SALT=$(dd if=/dev/urandom bs=1 count=6 2> /dev/null | base64)

while IFS=: read -a array line; do
	pass=$(mkpasswd -m sha-256 ${array[1]} ${SALT})
	useradd -p "${pass}" -u "${array[2]}" -g "${array[3]}" -m -d "${array[5]}" \
		-s "${array[6]}" "${array[0]}"
	#echo "name: ${array[0]}, pass: ${pass}, home: ${array[5]}, shell: ${array[6]}"
done < $FILE
